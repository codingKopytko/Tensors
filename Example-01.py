import tensorflow as tf
hello = tf.constant('Hello, TensorFlow!')

sess = tf.Session()
print(sess.run(hello))

a = tf.placeholder(tf.int16)
b = tf.placeholder(tf.int16)

add = tf.add(a, b)
mul = tf.multiply(a, b)
print(type(a))
print(type(add))
with tf.Session() as sess:
  # Run every operation with variable input
  print(  "Addition with variables: %i" % sess.run(add, feed_dict={a: 2, b: 3}))
  print( "Multiplication with variables: %i" % sess.run(mul, feed_dict={a: 2, b: 3}))

with tf.Session() as sess:
  matrix1 = tf.constant([[3., 3.], [4., 4.]])
  matrix2 = tf.constant([[2.], [2.]])

  matrix3 = tf.constant([2, 3, 4, 5])
  matrix4 = tf.constant([[2, 3, 4, 5], [6, 7, 3, 2]])
  matrix5 = tf.constant([[[1, 2], [3, 5], [4, 6], [5, 7]], [[6, 4], [7, 5], [3, 1], [2, 0]]])

  negMatrix1 = tf.negative(matrix1)
  print(sess.run(tf.abs(negMatrix1)))
  print(sess.run(matrix3))
  print(sess.run(matrix4))
  print()
  print(sess.run(matrix5))
  print(sess.run(tf.add(matrix1, matrix2)))
  product = tf.matmul(matrix1, matrix2)

  result = sess.run(product)
  print(result)
