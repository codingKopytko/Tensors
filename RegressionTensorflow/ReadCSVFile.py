import csv
import os


def csv_dict_reader(file_obj):
    values = []
    reader = csv.DictReader(file_obj, delimiter=',')
    for line in reader:
        float_dict = dict((key, float(value)) for key, value in line.items())
        values.append(float_dict)
    return values


def get_raw_values(path_to_csv_file):
    csv_path = path_to_csv_file
    with open(csv_path, "r") as f_obj:
        raw_values_dict = csv_dict_reader(f_obj)
    return raw_values_dict