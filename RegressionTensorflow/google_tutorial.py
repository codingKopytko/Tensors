import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data

mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)
a = mnist.test.labels[46]
b = mnist.test.images[44]

x = tf.placeholder(tf.float32, [None, 784])

W = tf.Variable(tf.zeros([784, 10]))
b = tf.Variable(tf.zeros([10]))
y = tf.nn.softmax(tf.matmul(x, W) + b)

y_ = tf.placeholder(tf.float32, [None, 10])

cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))
train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

init = tf.initialize_all_variables()

sess = tf.Session()
sess.run(init)
correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
valid_error_list = []
test_error_list = []
for i in range(1000):
  batch_xs, batch_ys = mnist.train.next_batch(100)
  sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})
  if i % 10 == 0:
    validationError = sess.run(accuracy, feed_dict={x: mnist.validation.images, y_: mnist.validation.labels})
    testError = sess.run(accuracy, feed_dict={x: mnist.test.images, y_: mnist.test.labels})
    valid_error_list.append(validationError)
    test_error_list.append(testError)
    if len(valid_error_list) > 2 and valid_error_list[-1] < valid_error_list[-2]:
      print(i)
      break

result = sess.run(tf.argmax(y, 1), feed_dict={x: mnist.validation.images})

validationError = sess.run(accuracy, feed_dict={x: mnist.validation.images, y_: mnist.validation.labels})
testError = sess.run(accuracy, feed_dict={x: mnist.test.images, y_: mnist.test.labels})

print('validation', " ".join(str(xx) for xx in valid_error_list))
print('test', " ".join(str(xx) for xx in test_error_list))

print('hello world', 'type of x', type(x), 'type of cross_entropy', type(cross_entropy))
