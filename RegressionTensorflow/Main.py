import numpy as np
import tensorflow as tf
import tensor_helper
from ReadCSVFile import get_raw_values
from SimpleGradientDescentOptimizerWrapper import SimpleGradientDescentOptimizerWrapper


labels = ["Open", "Ratio", "CloseLong", "CloseShort", "EvaluationLength", "RunsPerSample", "Ta", "TaLong", "TaShort"]
input_labels = ["Open", "Ratio", "CloseLong", "CloseShort"]
predicted_label = "Ta"
rawValues = get_raw_values("./rlot_ta_50k.csv")
np.random.shuffle(rawValues)

# We choose Open, Ratio, CloseLong and CloseShort as input values
formatted_input_data = tensor_helper.get_numpy_table_from_dict_list(rawValues, input_labels)
#formatted_input_data = np.array([[1],[2],[3],[4],[5],[6],[7],[8],[9]])
n_observations = formatted_input_data.shape[0]

testRatio = 0.8
number_of_testing_samples = int(n_observations * testRatio)
training_X_set = formatted_input_data[:number_of_testing_samples, :]
testing_X_set = formatted_input_data[number_of_testing_samples:, :]

# 'Ta' will be a predicted value
formatted_expected_values = tensor_helper.get_numpy_table_from_dict_list(rawValues, [predicted_label])
#formatted_expected_values = np.array([-3,-6,-9,-12,-15,-18,-21,-24,-27])
training_Y_set = formatted_expected_values[:number_of_testing_samples]
testing_Y_set = formatted_expected_values[number_of_testing_samples:]


n_epochs = 2000
with tf.Session() as sess:
    optimizer = SimpleGradientDescentOptimizerWrapper(training_X_set, training_Y_set, testing_X_set, testing_Y_set,
                                                      sess)
    sess.run(tf.global_variables_initializer())

    optimizer.learn_n_epochs(n_epochs, True)

    predicted_y = optimizer.get_predictions(training_X_set)

    zipped = zip(predicted_y, training_Y_set)
    for y1, y2 in zipped:
        pass
        # print("{:.4f},{:.4f}".format(y1, y2))
    optimizer.print_percentage_results()
    first_x = [x[0] for x in training_X_set]
    r_2 = tensor_helper.coefficient_of_determination(predicted_y, training_Y_set)
    print("r^2={}".format(r_2))
    weights = sess.run(optimizer.W)
    print("weights and bias: ", weights, optimizer.b.eval())

