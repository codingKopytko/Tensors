from SimpleGradientDescentOptimizerWrapper import SimpleGradientDescentOptimizerWrapper
import numpy as np
import tensorflow as tf


formatted_input_data = np.array([[1], [2], [3], [4], [5], [6], [7], [8], [9]])
n_observations = formatted_input_data.shape[0]

testRatio = 0.8
number_of_testing_samples = int(n_observations * testRatio)
training_X_set = formatted_input_data[:number_of_testing_samples, :]
testing_X_set = formatted_input_data[number_of_testing_samples:, :]

formatted_expected_values = np.array([-3, -6, -9, -12, -15, -18, -21, -24, -27])
training_Y_set = formatted_expected_values[:number_of_testing_samples]
testing_Y_set = formatted_expected_values[number_of_testing_samples:]

n_epochs = 2000
with tf.Session() as sess:
    optimizer = SimpleGradientDescentOptimizerWrapper(training_X_set, training_Y_set, testing_X_set, testing_Y_set, sess)

    sess.run(tf.global_variables_initializer())
    optimizer.learn_n_epochs(n_epochs, True)
    print(optimizer.get_predictions(testing_X_set))
