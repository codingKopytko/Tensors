from abc import ABCMeta, abstractmethod, abstractproperty, ABC


class OptimizerWrapper(ABC):
    def __init__(self, training_X_set, training_Y_set, testing_X_set, testing_Y_set, session):
        self.training_X_set = training_X_set
        self.training_Y_set = training_Y_set
        self.testing_X_set = testing_X_set
        self.testing_Y_set = testing_Y_set
        self.sess = session

    @abstractmethod
    def get_predictions(self, x_set):
        pass
