import numpy as np
import scipy
from statistics import mean
from scipy import stats


def get_numpy_table_from_dict_list(dictionary, list_of_labels):
    if len(list_of_labels) == 1:
        return np.array([x[list_of_labels[0]] for x in dictionary])
    columns = []
    for label in list_of_labels:
        columns.append([x[label] for x in dictionary])
    np_array = np.array(columns)
    transposed = np.transpose(np_array)
    return transposed


def squared_error(ys_orig, ys_line):
    return sum((ys_line - ys_orig) * (ys_line - ys_orig))


def coefficient_of_determination(ys_orig, ys_line):
    mean_value = ys_orig.mean()
    y_mean_line = [mean_value for y in ys_orig]
    squared_error_regr = squared_error(ys_orig, ys_line)
    squared_error_y_mean = squared_error(ys_orig, y_mean_line)
    return 1 - (squared_error_regr / squared_error_y_mean)
