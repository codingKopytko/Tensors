import tensorflow as tf

from OptimizerWrapper import OptimizerWrapper


class SimpleGradientDescentOptimizerWrapper(OptimizerWrapper):

    def __init__(self, training_X_set, training_Y_set, testing_X_set, testing_Y_set, session):
        super().__init__(training_X_set, training_Y_set, testing_X_set, testing_Y_set, session)

        self.W = tf.Variable(tf.random_normal([1, training_X_set.shape[1]]), name='weight')
        self.b = tf.Variable(tf.random_normal([1]), name='bias')

        self.x_placeholder = tf.placeholder(tf.float32, [None, training_X_set.shape[1]], name="X_for_computation")
        self.y_placeholder = tf.placeholder(tf.float32, [None], name="Y_for_computation")
        self.nr_of_samples_placeholder = tf.placeholder(tf.float32)

        sum_of_y = tf.add(tf.multiply(self.x_placeholder, self.W), self.b)
        self.Y_pred = tf.reduce_sum(sum_of_y, 1)
        self.cost = tf.reduce_sum(tf.pow(self.Y_pred - self.y_placeholder, 2)) / (self.nr_of_samples_placeholder - 1)

        self.averagePercentError = tf.reduce_mean(tf.abs(self.Y_pred - self.y_placeholder) * 100 / self.y_placeholder)

        learning_rate = 0.01
        self.optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(self.cost)

    def learn_n_epochs(self, n_epochs, print_results=False, batch_size=0):
        if batch_size == 0:
            batch_size = self.training_X_set.shape[0]
            # TODO implement batches
        for epoch_i in range(n_epochs):
            self.sess.run(self.optimizer, feed_dict={self.x_placeholder: self.training_X_set, self.y_placeholder: self.training_Y_set,
                                                     self.nr_of_samples_placeholder: self.training_X_set.shape[0]})

            training_cost = self.sess.run(
                self.cost, feed_dict={self.x_placeholder: self.training_X_set, self.y_placeholder: self.training_Y_set,
                                      self.nr_of_samples_placeholder: self.training_X_set.shape[0]})
            if print_results and epoch_i % (n_epochs / 10) == 0:
                print("training cost: ", training_cost)
                self.print_percentage_results()

    def print_percentage_results(self):
        trainingPercent = self.sess.run(self.averagePercentError, feed_dict={self.x_placeholder: self.training_X_set,
                                                                             self.y_placeholder: self.training_Y_set,
                                                                             self.nr_of_samples_placeholder: self.training_X_set.shape[0]})
        testingPercent = self.sess.run(self.averagePercentError, feed_dict={self.x_placeholder: self.testing_X_set,
                                                                            self.y_placeholder: self.testing_Y_set,
                                                                            self.nr_of_samples_placeholder: self.testing_X_set.shape[0]})
        print("Percent error for training: {:04.0f}% and testing: {:04.0f}%".format(trainingPercent, testingPercent))

    def get_predictions(self, x_set):
        predicted_y = self.sess.run(self.Y_pred, feed_dict={self.x_placeholder: x_set})
        return predicted_y

